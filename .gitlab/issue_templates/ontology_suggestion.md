# Issue Title (add to "Title" field ):
Should follow this pattern: Ontology suggestion: [ontology name] - [archetype/community cluster/(Sub-)SIG/institution/other]
e.g. `Ontology suggestion: Qudt - Metadata4Ing`

# Fill Ontology Metadata in YAML
You should provide some information on the ontology, for example, what is its title, what is it about, who created it, under what license has it been published?

This information helps us to decide whether the ontology can be added to the NFDI4Ing Terminology Service.

It is also important for NFDI4Ing Terminology Service users to understand what the scope of the ontology is. 

Some information about the ontology is obligatory (ontology_purl, title, id, preferredPrefix, license).

Please use the following schema to provide the information. If unavailable or unknown, just leave the respective line empty.

```yaml
ontology_purl: replace this text by the ontology's URI 
title: replace this text by the ontology's title
preferredPrefix: add the preferred prefix used by the ontology
id: repeat the ontologys's preferred prefix
license:
  label: provide the name of the ontology's license
  url: name the URL of the licence deed of the ontology
description: add the description of the ontology
homepage: provide a URL to a homepage related to the ontology if available, e.g. a documentation
mailing_list: provide the adress of a mailing list dedicated to the ontology
tracker: if the ontolgy is developed on an online platform like GitHub or GitLab provide the link to the ontology's issue tracker
creator:
  - provide a list of the ontology creators if known
classification:
  - collection:
    - NFDI4ING
```

Which DFG subjects are adressed by the ontology?
- [x] 4 Engineering Sciences
  - [ ] 41 Mechanical and Industrial Engineering
    - [ ] 401 Production Technology
      - [ ] 401-01 Metal-Cutting and Abrasive Manufacturing Engineering
      - [ ] 401-02 Primary Shaping and Reshaping Technology, Additive Manufacturing
      - [ ] 401-03 Joining and Separation Technology
      - [ ] 401-04 Plastics Engineering
      - [ ] 401-05 Production Systems, Operations Management, Quality Management and Factory Planning
      - [ ] 401-06 Production Automation
    - [ ] 402 Mechanics and Constructive Mechanical Engineering
      - [ ] 402-01 Engineering Design, Machine Elements, Product Development
      - [ ] 402-02 Mechanics
      - [ ] 402-03 Lightweight Construction, Textile Technology
      - [ ] 402-04 Acoustics
  - [ ] 42 Thermal Engineering/ Process Engineering
    - [ ] 403 Process Engineering, Technical Chemistry
      - [ ] 403-01 Chemical and Thermal Process Engineering
      - [ ] 403-02 Technical Chemistry
      - [ ] 403-03 Mechanical Process Engineering
      - [ ] 403-04 Biological Process Engineering
    - [ ] 404 Fluid Mechanics, Technical Thermodynamics and Thermal Energy Engineering
      - [ ] 404-01 Energy Process Engineering
      - [ ] 404-02 Technical Thermodynamics
      - [ ] 404-03 Fluid Mechanics
      - [ ] 404-04 Hydraulic and Turbo Engines and Piston Engines
  - [ ] 43 Material Science and Engineering
    - [ ] 405 Materials Engineering
      - [ ] 405-01 Metallurgical, Thermal and Thermomechanical Treatment of Materials
      - [ ] 405-02 Materials in Sintering Processes and Generative Manufacturing Processes
      - [ ] 405-03 Coating and Surface Technology
      - [ ] 405-04 Mechanical Properties of Metallic Materials and their Microstructural Origins
      - [ ] 405-05 Glass, Ceramics and Derived Composites
    - [ ] 406 Materials Science
      - [ ] 406-01 Synthesis and Properties of Functional Materials
      - [ ] 406-02 Biomaterials
      - [ ] 406-03 Thermodynamics and Kinetics as well as Properties of Phases and Microstructure of Materials
      - [ ] 406-04 Computer-Aided Design of Materials and Simulation of Materials Behaviour from Atomic to Microscopic Scale
  - [ ] 44 Computer Science, Systems and Electrical Engineering
    - [ ] 407 Systems Engineering
      - [ ] 407-01 Automation, Control Systems, Robotics, Mechatronics, Cyber Physical Systems
      - [ ] 407-02 Measurement Systems
      - [ ] 407-03 Microsystems
      - [ ] 407-04 Traffic and Transport Systems, Logistics, Intelligent and Automated Traffic
      - [ ] 407-05 Human Factors, Ergonomics, Human-Machine Systems
      - [ ] 407-06 Biomedical Systems Technology
    - [ ] 408 Electrical Engineering and Information Technology
      - [ ] 408-01 Electronic Semiconductors, Components, Circuits, Systems
      - [ ] 408-02 Communications, High-Frequency and Network Technology, Theoretical Electrical Engineering
      - [ ] 408-03 Electrical Energy Generation, Distribution, Application
    - [ ] 409 Computer Science
      - [ ] 409-01 Theoretical Computer Science
      - [ ] 409-02 Software Engineering and Programming Languages
      - [ ] 409-03 Security and Dependability
      - [ ] 409-04 Operating, Communication, Database and Distributed Systems
      - [ ] 409-05 Interactive and Intelligent Systems, Image and Language Processing, Computer Graphics and Visualisation
      - [ ] 409-06 Information Systems, Process and Knowledge Management
      - [ ] 409-07 Computer Architecture and Embedded Systems
      - [ ] 409-08 Massively Parallel and Data-Intensive Systems
  - [ ] 45 Construction Engineering and Architecture 
    - [ ] 410 Construction Engineering and Architecture
      - [ ] 410-01 Architecture, Building and Construction History, Construction Research, Sustainable Building Technology
      - [ ] 410-02 Urbanism, Spatial Planning, Transportation and Infrastructure Planning, Landscape Planning
      - [ ] 410-03 Construction Material Sciences, Chemistry, Building Physics
      - [ ] 410-04 Structural Engineering, Building Informatics and Construction Operation
      - [ ] 410-05 Applied Mechanics, Statics and Dynamics
      - [ ] 410-06 Geotechnics, Hydraulic Engineering