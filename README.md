# Terminology Service Issue Tracker

This is the issue tracker of the [NFDI4Ing Terminology Service](https://terminology.nfdi4ing.de/).

You can use it to

- report a bug,
- suggest an ontology to add to the service,
- suggest features or optimizations.

# How to add a suggestion

1. Go to the [issues page](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/terminology-service-issue-tracker/-/issues).
2. Create a new issue.
3. Choose an issue template (ontology_suggestion.md, bug.md) from the dropdown menu in the new issue.
4. Follow the instructions provided by the template.
5. Save the issue.

Your issue will be processed.
